#!/bin/bash

(./dnsd serv_config/valid/valid1.cnf &> /dev/null)&
pid=$!

echo "example.com A" > in.txt
dnsperf -p 5300 -c 10 -l 3 -d in.txt
rm in.txt

# Kill dns server process
kill -PIPE $pid