#!/bin/bash

test_dig ()
{
    total_tests_count=$(($total_tests_count+1))

    test_name=$1
    expected=$2
    answer_count=$3
    params=$4

    output=$(dig @127.0.0.1 -p 5300 $params)
    if [[ "$output" == *"$WARNING"* ]]; then
        echo "TEST '$test_name' KO : Warning in dig output, request is probably malformed"
    elif [[ "$output" == *"$expected"* ]]; then
        if [ $answer_count -gt 0 ]; then
            if [[ "$output" == *"ANSWER SECTION"* ]]; then
                echo "TEST '$test_name' OK"
            else
                echo "TEST '$test_name' KO : expected a non empty answer section"
            fi
        else
            echo "TEST '$test_name' OK"
        fi
    else
        echo "TEST '$test_name' KO : expected error code : $expected"
    fi
}

(./dnsd serv_config/valid/valid1.cnf &> /dev/null)&
pid=$!

test_dig "A record noerror" "NOERROR" "1" "example.com A"
test_dig "AAAA record noerror" "NOERROR" "1" "example.com AAAA"
test_dig "SOA record noerror" "NOERROR" "1" "example.com SOA"

test_dig "A record noerror" "NXDOMAIN" "0" "non-existing.com A"
test_dig "AAAA record noerror" "NXDOMAIN" "0" "non-existing.com AAAA"
test_dig "SOA record noerror" "NXDOMAIN" "0" "non-existing.com SOA"

# Kill dns server process
kill -PIPE $pid