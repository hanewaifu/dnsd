#!/bin/bash

test_func_conf ()
{
    total_tests_count=$(($total_tests_count+1))

    expected=$1
    test_name=$2
    file=$3

    ./dnsd -t "${file}" &> /dev/null
    ret=$?

    # Error case: file is not valid
    if [ "$expected" == "valid" ] && [ $ret -eq 0 ]; then
        echo -e "TEST '${test_name}' : OK"
        success_tests_count=$((success_tests_count + 1))
    elif [ "$expected" == "invalid" ] && [ $ret -ne 0 ]; then
        echo -e "TEST '${test_name}' : OK"
        success_tests_count=$((success_tests_count + 1))
    else
        echo -e "${test_name} : KO, expected $1"
        failed_tests_count=$((failed_tests_count + 1))
    fi
}

# Valid

test_func_conf "valid" "valid zone" "serv_config/valid/valid1.cnf"

# Invalid

test_func_conf "invalid" "wrong json1" "serv_config/invalid/invalid1.cnf"
test_func_conf "invalid" "wrong json2" "serv_config/invalid/invalid2.cnf"
test_func_conf "invalid" "wrong json3" "serv_config/invalid/invalid3.cnf"
test_func_conf "invalid" "wrong json4" "serv_config/invalid/invalid4.cnf"
test_func_conf "invalid" "wrong json5" "serv_config/invalid/invalid5.cnf"
