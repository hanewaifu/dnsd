#!/bin/sh

total_tests_count=0
failed_tests_count=0
success_tests_count=0

check() {
    binary=$1
    args=$2
    expected_status=$3
    expected_stdout=$4

    total_tests_count=$(($total_tests_count+1))

    stdout=$($binary $(echo $args))
    status_code=$?

    if [ ! $status_code -eq $expected_status ]; then
        failed_tests_count=$(($failed_tests_count+1))

        echo "TEST: $binary $args: FAILED: wrong status code\nexpected $expected_status, got $status_code"

    elif [ ! "$stdout" = "$expected_stdout" ]; then
        failed_tests_count=$(($failed_tests_count+1))

        echo "TEST: $binary $args: FAILED: wrong stdout\nexpected: \"$expected_stdout\"\ngot: \"$stdout\""

    else
        success_tests_count=$(($success_tests_count+1))
        echo "TEST: $binary $args: OK"

    fi
}

summary() {
    echo "Summary: Total: $total_tests_count, Success: $success_tests_count, Failed: $failed_tests_count"
    exit $failed_tests_count
}

build_dir=$1
src_tests_dir=$2

### TESTS

## EXAMPLE
# check "echo" "a b c" 0 "a b c"

check $build_dir/src/dnsd_config_test "" 0 ""
check $build_dir/src/dnsd_json_test "" 0 ""
check $build_dir/src/dnsd_tree_test "" 0 ""
check $build_dir/src/dnsd_requests_test "" 0 ""
check $build_dir/src/dnsd_resolver_test "" 0 ""
check $build_dir/src/dnsd_server_test "" 0 ""
check $build_dir/src/dnsd_utils_test "" 0 ""

summary
