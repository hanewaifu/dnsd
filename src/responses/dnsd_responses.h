#ifndef DNSD_RESPONSES_H
#define DNSD_RESPONSES_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/socket.h>
#include <unistd.h>

#include "requests/dnsd_requests.h"

struct dns_message build_dns_response(struct tree *tree,
                                      struct dns_message query);

void change_dns_response_flag(struct dns_message *message);

void add_dns_answer(struct tree *tree, struct dns_message *message, int index);

void add_dns_author(struct tree *tree,
                    struct dns_message *message,
                    struct record *records);

void add_dns_additional(struct tree *tree,
                        struct dns_message *message,
                        struct record *records);

#endif /* end of include guard: DNSD_RESPONSES_H */
