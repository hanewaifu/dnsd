#include "dnsd_responses.h"

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../requests/dnsd_requests.h"

struct dns_message build_dns_response(struct tree *tree,
                                      struct dns_message query)
{
    for (int i = 0; i < query.header.questions_count; i++)
    {
        add_dns_answer(tree, &query, i);
        // add_dns_author(tree, &query);
        // add_dns_additional(tree&query);
        query.questions[i].qtype = query.questions[i].qtype;
        query.questions[i].qclass = query.questions[i].qclass;
    }

    // change_dns_response_flag(&query);

    uint16_t mask = 1 << 15;
    query.header.flags = query.header.flags | mask;

    return query;
}

void header_htons(struct dns_header *header)
{
    (*header).query_id = htons((*header).query_id);
    (*header).questions_count = htons((*header).questions_count);
    (*header).answers_count = htons((*header).answers_count);
    (*header).author = htons((*header).author);
    (*header).additional = htons((*header).additional);
}

void change_dns_response_flag(struct dns_message *message)
{
    // header_htons(&(message->header));
    uint16_t mask = 1 << 0;
    message->header.flags += mask;
    // TODO: error code
}

uint16_t int_to_int(int k)
{
    return (k == 0 || k == 1 ? k : ((k % 2) + 10 * int_to_int(k / 2)));
}

void add_dns_answer(struct tree *tree, struct dns_message *message, int index)
{
    struct record *records = NULL;
    char name[] = "example.com";
    int record_count = get_record(tree,
                                  name, // message->questions[index].qname,
                                  message->questions[index].qtype,
                                  &records);
    // TODO: handle record count
    message->header.answers_count = (uint16_t) int_to_int(record_count);

    if (message->answer == NULL)
        message->answer = records;
    else
    {
        struct record *tmp = message->answer;
        while (tmp->next)
            tmp = tmp->next;
        tmp->next = records;
    }

    struct record *tmp = records;
    while (tmp)
    {
        tmp->type = tmp->type;
        tmp->class = tmp->class;
        tmp->TTL = tmp->TTL;
        tmp->len = tmp->len;
        tmp = tmp->next;
    }
}

/*
void add_dns_author(struct tree* tree, struct dns_message *message,
        struct record *records)
{
    //TODO
    return;
}

void add_dns_additional(struct tree* tree, struct dns_message *message,
        struct record *records)
{
    //TODO
    return;
}
*/
