#ifndef ENCODE_RESPONSE_H
#define ENCODE_RESPONSE_H

#include "requests/dnsd_requests.h"

void *encode_response(struct dns_message dns_msg, size_t *size);

void encode_dns_headers(struct dns_header headers, void *raw, uint16_t *offset);

void encode_dns_questions(int qdcount,
                          struct dns_question *questions,
                          void *raw,
                          uint16_t *offset);

void encode_dns_answer(int ancount,
                       struct record *answers,
                       void *raw,
                       uint16_t *offset);

#endif
