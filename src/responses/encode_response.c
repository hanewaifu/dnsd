#include "responses/encode_response.h"

#include <arpa/inet.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "requests/dnsd_requests.h"

#define UDP_MTU 512

void *encode_response(struct dns_message dns_msg, size_t *size)
{
    void *bits = malloc(UDP_MTU + 1024);
    uint16_t offset = 0;

    encode_dns_headers(dns_msg.header, bits, &offset);

    encode_dns_questions(
        dns_msg.header.questions_count, dns_msg.questions, bits, &offset);

    // encode_dns_answer(dns_msg.header.answers_count, dns_msg.answer, bits,
    // &offset);

    *size = offset;
    return bits;
}

void encode_dns_headers(struct dns_header headers, void *raw, uint16_t *offset)
{
    uint16_t *bits = raw;
    bits[(*offset)++] = htons(headers.query_id);
    bits[(*offset)++] = htons(headers.flags);
    bits[(*offset)++] = htons(headers.questions_count);
    bits[(*offset)++] = htons(headers.answers_count);
    bits[(*offset)++] = htons(headers.author);
    bits[(*offset)++] = htons(headers.additional);
    (*offset) *= 2;
}

void domain_name_to_bits_2(char *qname, void *raw, uint16_t *b)
{
    uint8_t *bits = raw;
    bool tmp = true;
    bool hit = false;
    for (size_t i = 0; i < strlen(qname); ++i)
    {
        char c = qname[i];
        uint8_t count = 0;
        if ((!hit && i == 0) || c == '.')
        {
            if (tmp)
            {
                tmp = false;
                count = qname[i];
                bits[(*b)++] = count;
                continue;
            }
            if (!hit && i == 0)
            {
                hit = true;
                --i;
            }
            size_t temp_i = i + 1;
            while (temp_i < strlen(qname) && qname[temp_i] != '.')
            {
                count++;
                temp_i++;
            }
        }
        else
            count = qname[i];

        bits[(*b)++] = count;
    }
}

void encode_dns_questions(int qdcount,
                          struct dns_question *questions,
                          void *raw,
                          uint16_t *offset)
{
    uint8_t *bits = raw;
    for (int i = 0; i < qdcount; ++i)
    {
        struct dns_question cur_question = questions[i];
        // QNAME
        domain_name_to_bits_2(cur_question.qname, bits, offset);
        // Empty byte after QNAME
        bits[(*offset)++] = 0;
        // QTYPE & QCLASS
        uint16_t *flags = (uint16_t *) ((uint8_t *) raw + *offset);
        flags[0] = htons(cur_question.qtype);
        *offset += 2;
        flags[1] = htons(cur_question.qclass);
        *offset += 2;
    }
}

void encode_dns_answer(int ancount,
                       struct record *answers,
                       void *raw,
                       uint16_t *offset)
{
    uint8_t *bits = raw;
    for (int i = 0; i < ancount; ++i)
    {
        struct record cur_r = answers[i];
        domain_name_to_bits_2(cur_r.name, bits, offset);
        bits[(*offset)++] = 0;

        uint16_t *cur = (uint16_t *) ((uint8_t *) raw + *offset);
        cur[0] = htons(cur_r.type);
        *offset += 2;
        cur[1] = htons(cur_r.class);
        *offset += 2;
        uint32_t *ttl = (uint32_t *) ((uint8_t *) raw + *offset);
        ttl[0] = htonl(cur_r.TTL);
        *offset += 4;

        cur[4] = htons(cur_r.len);
        *offset += 2;

        uint8_t *val = (uint8_t *) cur_r.data;
        for (size_t i = 0; i < cur_r.len; ++i)
            bits[(*offset)++] = val[i];
    }
}
