#ifndef STRING_H
#define STRING_H

#include <stdbool.h>
#include <stdio.h>

typedef struct
{
    char *arr;
    size_t size;
    size_t capacity;
} string;

/**
** \brief                           Initializes a string
** \return                          String initialized
*/
string *string_init(void);

/**
** \brief                           Frees the memory inside a string
** \param s                         The string to free
** \return                          void
*/
void string_free(string *s);

void string_flush(string *s);

/**
** \brief                           Resizes a string'b arr argument
** \param b                         The string to alter
** \return                          The modified string
*/
void string_resize(string *b);

/**
** \brief                           Adds a char to a string
** \param b                         The string to alter
** \param c                         The character to add to the string
** \return                          The modified string
*/
void string_add_char(string *b, char c);

/**
** \brief                           Adds another string to a string object
** \param b                         The string to alter
** \param s2                        The string to add to the main string
** \return                          The modified string
*/
void string_add_str(string *b, char *s2);

#endif
