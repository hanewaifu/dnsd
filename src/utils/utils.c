#define _GNU_SOURCE

#include "utils.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sigint_callback(int sig)
{
    if (sig == SIGINT)
    {
        printf("\nExited server successfully.\n");
        exit(0);
    }
}

void catch_sigints()
{
    struct sigaction si = { .sa_handler = sigint_callback,
                            .sa_flags = SA_NODEFER };
    sigaction(SIGINT, &(si), 0);
}

char *read_file(const char *path)
{
    FILE *fp = fopen(path, "r");
    if (fp == NULL)
    {
        fprintf(stderr,
                "File doesn't exists or isn't usable ERRNO returned: %d",
                errno);
        exit(1);
    }

    fseek(fp, 0, SEEK_END);
    size_t size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // creating buffer
    char *buffer = malloc(size + 1);
    int ret = fread(buffer, 1, size, fp);
    if (ret == 0)
    {
        fprintf(stderr, "Can't read any byte of the file");
        free(buffer);
        exit(1);
    }
    buffer[ret] = '\0';
    ret = fclose(fp);
    if (ret == EOF)
    {
        fprintf(stderr, "Can't close the file");
    }
    return buffer;
}