#ifndef UTILS_H
#define UTILS_H

/**
 * \brief                   Catches a sigint which will have to cleanly stop the
 *server \return
 **/
void catch_sigints();

/**
 * \brief                   Reads a file's contents
 * \return                  The contents of the file
 **/
char *read_file(const char *path);

#endif /* !UTILS_H */
