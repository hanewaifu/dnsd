#include "config/server_config.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Initialization of the server configuration, set the default IP, the
 * defaults ports and create the tree */

server_config *server_config_init()
{
    server_config *config = malloc(sizeof(server_config));
    // Default values
    const char *default_ip = "127.0.0.1";
    config->ip = malloc(strlen(default_ip) + 1);
    strcpy(config->ip, default_ip);
    config->tcp_port = 53;
    config->udp_port = 53;
    config->options = NULL;
    config->tree = NULL;
    return config;
}

/* Free the server configuration object. Free all the malloc'ed members of
 * server_config then free itself.
 */

void server_config_free(server_config *config)
{
    if (config)
    {
        if (config->ip)
            free(config->ip);
        if (config->options)
            options_free(config->options);
        free(config);
    }
}
