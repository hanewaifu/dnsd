#ifndef SERVER_CONFIG_H
#define SERVER_CONFIG_H

#include <tree/tree.h>

#include "config/options.h"

/**
 * \brief                   Server configuration object
 * \param ip                Server ip (ipv4 or ipv6)
 * \param tcp_port          TCP port on which the server will listen
 * \param udp_port          UDP port on which the server will listen
 **/
typedef struct
{
    options *options;
    char *ip;
    int tcp_port;
    int udp_port;
    struct tree *tree;
} server_config;

/**
 * \brief                   Creates a server_config object
 * \return                  The newly created server_config object
 **/
server_config *server_config_init();

/**
 * \brief                   Frees the server config
 * \return
 **/
void server_config_free(server_config *config);

#endif /* !SERVER_CONFIG_H */
