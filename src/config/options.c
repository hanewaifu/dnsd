#include "options.h"

#include <stdlib.h>
#include <string.h>

/* Initializer for the options, set the default values */

options *options_init()
{
    options *opt = malloc(sizeof(options));
    opt->check = false;
    opt->verbose = false;
    opt->filepath = NULL;
    return opt;
}

/* Destructor for the options, free al the options allocated spaces then free
 * itself
 */

void options_free(options *options)
{
    if (options)
    {
        if (options->filepath)
            free(options->filepath);
        free(options);
    }
}

/* Options parser, return an options struct with all the parsed elements */

options *parse_options(int argc, char *argv[])
{
    options *options = options_init();
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--check") == 0)
            options->check = true;
        else if (strcmp(argv[i], "-v") == 0
                 || strcmp(argv[i], "--verbose") == 0)
            options->verbose = true;
        else
        {
            options->filepath = malloc(strlen(argv[i]) + 1);
            strcpy(options->filepath, argv[i]);
        }
    }
    return options;
}
