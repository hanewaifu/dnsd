#include "record_values.h"

#include <arpa/inet.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *record_val_to_bits(enum qtype type, string *val, size_t *b)
{
    switch (type)
    {
        case A:
            return A_init(val, b);
        case AAAA:
            return AAAA_init(val, b);
        case SOA:
            return SOA_init(val, b);
        default:
            return NULL;
    }
}

int power(int a, int b)
{
    int res = 1;
    for (; b > 0; --b)
        res *= a;
    return res;
}

int binary_to_decimal_unsigned(string *in)
{
    int res = 0;
    for (int i = in->size - 1; i >= 0; --i)
    {
        char c = in->arr[i];
        res += (c - '0') * power(2, in->size - i - 1);
    }
    return res;
}

int binary_to_decimal(string *in)
{
    int res = binary_to_decimal_unsigned(in);
    if (res > 127 && res < 256)
        res -= 256;
    return res;
}

string *hexa_to_binary(string *hex)
{
    char *binary[16] = { "0000", "0001", "0010", "0011", "0100", "0101",
                         "0110", "0111", "1000", "1001", "1010", "1011",
                         "1100", "1101", "1110", "1111" };
    const char digits[16] = "0123456789abcdef";
    string *res = string_init();
    for (size_t i = 0; i < hex->size; ++i)
    {
        char c = tolower(hex->arr[i]);
        char *v = strchr(digits, c);
        if (v)
            string_add_str(res, binary[v - digits]);
        // strcat(res, binary[v - digits]);
    }
    return res;
}

void get_soa_values(string *s,
                    string **mname,
                    string **rname,
                    string **serial,
                    string **refresh,
                    string **retry,
                    string **expire,
                    string **minimum)
{
    int cur = 1;
    for (size_t i = 0; s->arr[i]; ++i)
    {
        if (s->arr[i] == ' ')
            ++cur;
        else
        {
            if (cur == 1)
                string_add_char(*mname, s->arr[i]);
            else if (cur == 2)
                string_add_char(*rname, s->arr[i]);
            else if (cur == 3)
                string_add_char(*serial, s->arr[i]);
            else if (cur == 4)
                string_add_char(*refresh, s->arr[i]);
            else if (cur == 5)
                string_add_char(*retry, s->arr[i]);
            else if (cur == 6)
                string_add_char(*expire, s->arr[i]);
            else if (cur == 7)
                string_add_char(*minimum, s->arr[i]);
        }
    }
}

void *A_init(string *val, size_t *b)
{
    uint8_t *bits = malloc((4) * sizeof(uint8_t));
    string *tampon = string_init();
    for (size_t i = 0; val->arr[i]; ++i)
    {
        char c = val->arr[i];
        if (c == '.' || i == val->size - 1)
        {
            if (i == val->size - 1)
                string_add_char(tampon, c);
            bits[(*b)++] = atoi(tampon->arr);
            string_flush(tampon);
        }
        else
            string_add_char(tampon, c);
    }
    string_free(tampon);
    return bits;
}

void *AAAA_init(string *val, size_t *b)
{
    void *res = malloc((16) * sizeof(uint16_t));
    uint16_t *bits = res;
    int cur_b = 0;
    string *tampon = string_init();
    for (size_t i = 0; val->arr[i]; ++i)
    {
        char c = val->arr[i];
        if (c == ':' || i == val->size - 1)
        {
            if (i == val->size - 1)
                string_add_char(tampon, c);
            string *rdata_temp = hexa_to_binary(tampon);
            bits[cur_b++] = htons(binary_to_decimal(rdata_temp));
            *b += 2;
            string_flush(tampon);
        }
        else
            string_add_char(tampon, c);
    }
    string_free(tampon);
    return res;
}

void domain_name_to_bits(string *qname, void *raw, size_t *b)
{
    uint8_t *bits = raw;
    bool hit = false;
    for (size_t i = 0; i < qname->size; ++i)
    {
        char c = qname->arr[i];
        uint8_t count = 0;
        if ((!hit && i == 0) || c == '.')
        {
            if (!hit && i == 0)
            {
                hit = true;
                --i;
            }
            size_t temp_i = i + 1;
            while (temp_i < qname->size && qname->arr[temp_i] != '.')
            {
                count++;
                temp_i++;
            }
        }
        else
            count = qname->arr[i];

        bits[(*b)++] = count;
    }
}

void *SOA_init(string *val, size_t *b)
{
    void *res = malloc((val->size * 2 + 1) * sizeof(uint8_t));

    string *mname = string_init(), *rname = string_init(),
           *serial = string_init(), *refresh = string_init(),
           *retry = string_init(), *expire = string_init(),
           *minimum = string_init();
    get_soa_values(
        val, &mname, &rname, &serial, &refresh, &retry, &expire, &minimum);

    domain_name_to_bits(mname, res, b);

    domain_name_to_bits(rname, res, b);

    uint32_t *bits = (uint32_t *) ((uint8_t *) res + *b);
    bits[0] = htonl(atoi(serial->arr));
    *b += 4;
    bits[1] = htonl(atoi(refresh->arr));
    *b += 4;
    bits[2] = htonl(atoi(retry->arr));
    *b += 4;
    bits[3] = htonl(atoi(expire->arr));
    *b += 4;
    bits[4] = htonl(atoi(minimum->arr));
    *b += 4;

    string_free(mname);
    string_free(rname);
    string_free(serial);
    string_free(refresh);
    string_free(retry);
    string_free(expire);
    string_free(minimum);

    return res;
}
