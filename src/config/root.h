#ifndef ROOT_H
#define ROOT_H

#include <tree/tree.h>

void root_record(struct tree *t);

#endif /* !ROOT_H */
