#include "root.h"

#include <requests/dnsd_requests.h>
#include <stdlib.h>
#include <string.h>

static struct record *
build_root_record(struct record *record, enum qtype type, char *data)
{
    uint16_t class;
    memcpy(&class, "IN", 2);

    struct record *new_record =
        record_init(type, class, 3600000, data, ".", strlen(data));
    if (!record)
    {
        return new_record;
    }
    new_record->next = record;
    return new_record;
}

// Root record from https://github.com/NLnetLabs/ldns/blob/develop/drill/root.c

void root_record(struct tree *t)
{
    struct record *root = NULL;
    root = build_root_record(root, A, "198.41.0.4");
    root = build_root_record(root, AAAA, "2001:503:BA3E::2:30");
    root = build_root_record(root, A, "192.228.79.201");
    root = build_root_record(root, A, "192.33.4.12");
    root = build_root_record(root, A, "128.8.10.90");
    root = build_root_record(root, A, "192.203.230.10");
    root = build_root_record(root, A, "192.5.5.241");
    root = build_root_record(root, AAAA, "2001:500:2F::F");
    root = build_root_record(root, A, "192.112.36.4");
    root = build_root_record(root, A, "128.63.2.53");
    root = build_root_record(root, AAAA, "2001:500:1::803F:235");
    root = build_root_record(root, A, "192.36.148.17");
    root = build_root_record(root, A, "192.58.128.30");
    root = build_root_record(root, AAAA, "2001:503:C27::2:30");
    root = build_root_record(root, A, "193.0.14.129");
    root = build_root_record(root, AAAA, "2001:7FD::1");
    root = build_root_record(root, A, "199.7.83.42");
    root = build_root_record(root, AAAA, "2001:500:3::42");
    root = build_root_record(root, A, "202.12.27.33");
    root = build_root_record(root, A, "2001:DC3::35");

    add_record_root(t, root);
}
