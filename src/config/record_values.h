#ifndef RECORD_VALUES_H
#define RECORD_VALUES_H

#include <stdint.h>

#include "requests/dnsd_requests.h"
#include "utils/string.h"

void *record_val_to_bits(enum qtype type, string *val, size_t *b);

void *A_init(string *val, size_t *b);

void *AAAA_init(string *val, size_t *b);

void *SOA_init(string *val, size_t *b);

#endif