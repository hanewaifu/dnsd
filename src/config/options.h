#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdbool.h>

/* Options struct, it holds all the possible
 * options that can be passed as arguments */

typedef struct
{
    char *filepath;
    bool verbose;
    bool check;
} options;

options *options_init();

void options_free(options *options);

options *parse_options(int argc, char *argv[]);

#endif
