#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "config/options.h"
#include "config/server_config.h"
#include "json/json-parser.c"
#include "json/json.h"
#include "server/server.h"
#include "utils/utils.h"

int main(int argc, char *argv[])
{
    options *options = parse_options(argc, argv);
    if (options->filepath == NULL)
        errx(1, "Usage: ./dnsd { config-file }");

    const char *input_path = options->filepath;

    server_config *serv_config = parse_server_config(input_path);

    serv_config->options = options;

    if (options->check)
    {
        printf("All configuration files are valid.\n");
        server_config_free(serv_config);
        exit(0);
    }

    if (options->verbose)
    {
        printf("Server ip: %s\n", serv_config->ip);
        printf("Server udp port: %d\n", serv_config->udp_port);
        printf("Server tcp port: %d\n", serv_config->tcp_port);
    }

    catch_sigints();

    launch_server(serv_config);

    return EXIT_SUCCESS;
}
