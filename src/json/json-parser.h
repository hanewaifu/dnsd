#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include <tree/tree.h>

struct tree *json_parser(char *file);

#endif // JSON_PARSER_H
