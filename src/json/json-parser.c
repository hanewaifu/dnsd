#include "json-parser.h"

#include <err.h>
#include <errno.h>
#include <json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config/record_values.h"
#include "json/json-parser.h"

/* Generate a tree struct from a buffer. It uses the json c library.
 * It will read the whole file and stop on an error, like if a record
 * is empty or if the json format isn't respected */

static struct tree *generate_tree(char *buffer)
{
    struct json_object *request;
    request = json_tokener_parse(buffer);
    struct tree *tree = tree_init();
    size_t len;
    struct json_object *item;
    struct json_object *name;
    struct json_object *type;
    struct json_object *ttl;
    struct json_object *data;
    struct json_object *class;

    json_object_object_foreach(request, key, val)
    {
        key = key;
        len = json_object_array_length(val);

        for (size_t i = 0; i < len; i++)
        {
            item = json_object_array_get_idx(val, i);

            json_object_object_get_ex(item, "name", &name);
            if (!name)
                err(1, "No record name specified");
            char *get_name = malloc(strlen(json_object_get_string(name)) + 1);
            strcpy(get_name, json_object_get_string(name));
            if (strlen(get_name) == 0)
                err(1, "Name can't be empty");

            json_object_object_get_ex(item, "type", &type);
            if (!type)
                err(1, "No record type specified");
            uint16_t get_type = json_object_get_int(type);
            if ((get_type < 1 || get_type > 15) && get_type != 28)
            {
                err(1, "Zone file: invalid record type: %d\n", get_type);
            }

            json_object_object_get_ex(item, "TTL", &ttl);
            if (!ttl)
                err(1, "No record TTL specified");
            uint32_t get_ttl = json_object_get_int(ttl);

            json_object_object_get_ex(item, "class", &class);
            uint16_t get_class = json_object_get_int(class);

            json_object_object_get_ex(item, "data", &data);
            if (!data)
                err(1, "No record data specified");
            /*char *get_data = malloc(strlen(json_object_get_string(data)) + 1);
            strcpy(get_data, json_object_get_string(data));*/
            /*if (strlen(get_data) > 512)
                err(1, "Data can't be longer than 512 bytes");*/
            string *str = string_init();
            string_add_str(str, (char *) json_object_get_string(data));
            size_t b = 0;
            void *get_data = record_val_to_bits(get_type, str, &b);

            struct record *record = record_init(
                get_type, get_class, get_ttl, get_data, get_name, b);
            add_record(tree, get_name, record);
            // free(get_data);
            free(get_name);
        }
    }

    free(buffer);
    return tree;
}

/* Helpful function to create the tree, it opnes the file, check that it is
 * actually readable, check the size and close it when the tree generator
 * doesn't need the file anymore */

struct tree *json_parser(char *file)
{
    FILE *fp = fopen(file, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "File doesn't exists or isn't usable\n");
        fprintf(stderr, "ERRNO returned: %d\n", errno);
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    size_t size = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // creating buffer
    char *buffer = calloc(1, size + 1);
    int ret = fread(buffer, 1, size, fp);
    if (ret == 0)
    {
        fprintf(stderr, "Can't read any bite of the file");
        return NULL;
    }

    ret = fclose(fp);

    if (ret == EOF)
    {
        fprintf(stderr, "Can't close the file");
    }

    return generate_tree(buffer);
}
