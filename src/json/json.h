#ifndef JSON_H
#define JSON_H

#include <config/server_config.h>
#include <json.h>

/**
 * \brief                   Parses a server configuration file
 * \param path              Path to the configuration file
 * \return                  The server config resulting from the parsing
 **/
server_config *parse_server_config(const char *path);

#endif /* !JSON_H */
