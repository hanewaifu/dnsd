#include "json.h"

#include <config/root.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tree/tree.h>
#include <utils/utils.h>

#include "json-parser.h"

/* Json parser for the configuration file, it takes the path as argument. It
 * read all the different informations then it changes, if needed the default
 * configuration, i.e. the TCP/UDP port.
 */

server_config *parse_server_config(const char *path)
{
    server_config *config = server_config_init();

    char *buffer = read_file(path);

    struct json_object *jConfig = json_tokener_parse(buffer), *jIp, *jTCPp,
                       *jUDPp, *jZones;
    if (jConfig == NULL)
        errx(EXIT_FAILURE, "Server configuration not in valid JSON.");

    json_object_object_get_ex(jConfig, "ip", &jIp);
    json_object_object_get_ex(jConfig, "udp_port", &jUDPp);
    json_object_object_get_ex(jConfig, "tcp_port", &jTCPp);
    json_object_object_get_ex(jConfig, "zones", &jZones);

    if (jZones)
    {
        size_t len = json_object_array_length(jZones);
        for (size_t i = 0; i < len; i++)
        {
            struct json_object *jItem = json_object_array_get_idx(jZones, i),
                               *jZoneName, *jZonePath;
            (void) jZoneName;
            json_object_object_get_ex(jItem, "path", &jZonePath);
            char *zonePath =
                malloc(strlen(json_object_get_string(jZonePath)) + 1);
            strcpy(zonePath, json_object_get_string(jZonePath));
            config->tree = json_parser(zonePath);
            free(zonePath);
            root_record(config->tree);
        }
    }
    if (jIp)
    {
        const char *ip = json_object_get_string(jIp);
        config->ip = realloc(config->ip, strlen(ip) + 1);
        strcpy(config->ip, ip);
    }
    if (jTCPp)
    {
        const char *tcpPort = json_object_get_string(jTCPp);
        config->tcp_port = atoi(tcpPort);
    }
    if (jUDPp)
    {
        const char *udpPort = json_object_get_string(jUDPp);
        config->udp_port = atoi(udpPort);
    }

    // Free memory
    json_object_put(jConfig);
    free(buffer);

    return config;
}
