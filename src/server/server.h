#ifndef SERVER_H
#define SERVER_H

#include <config/server_config.h>

#define TCP_THREAD_CAP 2
#define UDP_THREAD_CAP 4

/**
 * \brief                   Launches the server, both on UDP and TCP
 * \param config            Server configuration object
 * \return
 **/
void launch_server(server_config *config);

#endif /* !SERVER_H */
