#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#define UDP_EPOLL_MAX_EV 10000
#define UDP_MTU 512

#include "config/server_config.h"

/**
 * \brief                   Callback which launches the server on UDP port
 *(passed to thread_create call) \param args              Server configuration
 *object casted as void pointer \return
 **/
void *launch_udp_server(void *args);

/**
 * \brief                   Launches the event loop
 * \param socket            UDP socket
 * \return
 **/
void listen_udp(int socket, server_config *config);

/**
 * \brief                   Receives messages via UDP
 * \param socket            UDP socket
 * \return
 **/
void receive_udp_message(int socket, server_config *config);

#endif /* !UDP_SERVER_H */