#ifndef TRANSPORT_H
#define TRANSPORT_H

/**
 * \brief                   Transport protocol enum
 **/
typedef enum
{
    TCP,
    UDP
} T_PROTOCOL;

#endif
