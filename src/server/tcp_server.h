#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#define TCP_EPOLL_MAX_EV 10000
#define TCP_MTU 4096

#include "config/server_config.h"

/**
 * \brief                   Callback which launches the server on TCP port
 *(passed to thread_create call) \param args              Server configuration
 *object casted as void pointer \return
 **/
void *launch_tcp_server(void *args);

/**
 * \brief                   Launches the event loop
 * \param socket            TCP socket
 * \return
 **/
void listen_tcp(int socket, server_config *config);

/**
 * \brief                   Accepts a new connection made via TCP
 * \param epoll_socket      Epoll socket
 * \param server_socket     TCP server socket
 * \return
 **/
void accept_tcp_client(int epoll_socket, int server_socket);

/**
 * \brief                   Receives messages via TCP
 * \param epoll_socket      Epoll socket
 * \param client_socket     Client socket
 * \return
 **/
void receive_tcp_message(int epoll_socket,
                         int client_socket,
                         server_config *config);

#endif /* !TCP_SERVER_H */
