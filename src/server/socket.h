#ifndef SOCKET_H
#define SOCKET_H

#include <config/server_config.h>

#include "transport.h"

/**
 * \brief                   Sets a socket as non blocking
 * \param socket            Socket
 * \return
 **/
void sock_set_non_blocking(int socket);

/**
 * \brief                   Wrapper around 'getaddrinfo' which binds a socket
 * \param config            Server configuration object
 * \param proto             Transport protocol (UDP or TCP)
 * \return                  The newly created socket
 **/
int get_addrinfo(server_config *config, T_PROTOCOL proto);

#endif
