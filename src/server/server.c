#include "server.h"

#include <err.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>

#include "tcp_server.h"
#include "udp_server.h"

/*
 *  Launch the epoll server. It uses multithreading and it is listening on both
 *  TCP and UDP ports
 */

void launch_server(server_config *config)
{
    pthread_t th_udp;
    int i, p;
    for (i = 0; i < UDP_THREAD_CAP; ++i)
    {
        p = pthread_create(&th_udp, NULL, launch_udp_server, config);
        if (config->options->verbose)
            printf("UDP server launched on thread %d\n", i + 1);
        if (p < 0 || errno == EAGAIN)
            err(1, "thread creation failed");
    }

    pthread_t th_tcp;
    int j;
    for (j = 0; j < TCP_THREAD_CAP; ++j)
    {
        p = pthread_create(&th_tcp, NULL, launch_tcp_server, config);
        if (config->options->verbose)
            printf("TCP server launched on thread %d\n", i + j + 1);
        if (p < 0 || errno == EAGAIN)
            err(1, "thread creation failed");
    }

    while (true)
    {
    }
}
