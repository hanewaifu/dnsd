#define _GNU_SOURCE

#include "server/tcp_server.h"

#include <err.h>
#include <errno.h>
#include <json/json-parser.h>
#include <netdb.h>
#include <requests/dnsd_requests.h>
#include <responses/dnsd_responses.h>
#include <responses/encode_response.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "socket.h"
#include "tcp_server.h"

/*
 *  Start a TCP server
 */

void *launch_tcp_server(void *args)
{
    server_config *config = (server_config *) args;
    int tcp_sock = get_addrinfo(config, TCP);
    sock_set_non_blocking(tcp_sock);

    listen_tcp(tcp_sock, config);

    return 0;
}

/*
 *  Main server function working around an infinite loop so it can listen
 *  on TCP port while the process is alive
 */

void listen_tcp(int socket, server_config *config)
{
    int s = listen(socket, SOMAXCONN);
    if (s < 0)
        err(1, "tcp listen failed");

    int event_count, i;
    struct epoll_event event, events[TCP_EPOLL_MAX_EV];
    int epoll_fd = epoll_create(TCP_EPOLL_MAX_EV);
    if (epoll_fd == -1)
        err(1, "epoll fd creation failed");

    event.events = EPOLLIN;
    event.data.fd = socket;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket, &event) < 0)
    {
        close(epoll_fd);
        err(1, "epoll_ctl add failed");
    }

    while (true)
    {
        event_count = epoll_wait(epoll_fd, events, TCP_EPOLL_MAX_EV, 1000);
        if (event_count == 0)
            continue;
        for (i = 0; i < event_count; ++i)
        {
            if (events[i].data.fd == socket)
                accept_tcp_client(epoll_fd, socket);
            else
                receive_tcp_message(epoll_fd, events[i].data.fd, config);
        }
    }
}

/*
 *  Accept TCP connection with a client through the server socket which is given
 *  as an argument
 */

void accept_tcp_client(int epoll_socket, int server_socket)
{
    int client_socket;
    struct sockaddr_in cliaddr;
    socklen_t socklen = sizeof(struct sockaddr_in);
    struct epoll_event ev;

    client_socket =
        accept(server_socket, (struct sockaddr *) &cliaddr, &socklen);
    if (client_socket < 0)
        err(1, "tcp accept error");

    sock_set_non_blocking(client_socket);

    ev.events = EPOLLIN;
    ev.data.fd = client_socket;

    epoll_ctl(epoll_socket, EPOLL_CTL_ADD, client_socket, &ev);
}

/*
 *  Receive a message through the socket, directly sent to the epoll server so
 *  it can be parsed
 */

void receive_tcp_message(int epoll_socket,
                         int client_socket,
                         server_config *config)
{
    uint16_t buff_len;
    ssize_t size = read(client_socket, &buff_len, 2);

    if (size <= 0)
        return;

    buff_len = ntohs(buff_len);
    char *input = malloc(sizeof(char) * buff_len);
    size = read(client_socket, input, buff_len);

    // TODO read input in multiple blocks
    if (size > 0)
    {
        // TODO Parse request call here
        // TODO for now send back request

        struct dns_message query = parse_dns_query(input);

        struct dns_message response = build_dns_response(config->tree, query);
        size_t sizeResp;
        void *bits = encode_response(response, &sizeResp);
        // TODO for now send back request
        uint16_t size = htons(sizeResp);
        write(client_socket, &size, sizeof(uint16_t));
        write(client_socket, bits, sizeResp);

        free(bits);
        free_dns_request(query);

        struct epoll_event ev;
        ev.data.fd = client_socket;
        epoll_ctl(epoll_socket, EPOLL_CTL_DEL, client_socket, &ev);
        close(client_socket);
    }
    free(input);
}
