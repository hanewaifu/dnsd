#define _GNU_SOURCE

#include "server/udp_server.h"

#include <json/json-parser.h>
#include <netdb.h>
#include <requests/dnsd_requests.h>
#include <responses/dnsd_responses.h>
#include <responses/encode_response.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "socket.h"
#include "udp_server.h"

/*
 *  Start a UDP server
 */

void *launch_udp_server(void *args)
{
    server_config *config = (server_config *) args;
    int udp_sock = get_addrinfo(config, UDP);
    sock_set_non_blocking(udp_sock);

    listen_udp(udp_sock, config);

    return 0;
}

/*
 *  Main server function working around an infinite loop so it can listen
 *  on UDP port while the process is alive
 */
void listen_udp(int socket, server_config *config)
{
    int event_count, i;
    struct epoll_event event, events[UDP_EPOLL_MAX_EV];
    int epoll_fd = epoll_create(UDP_EPOLL_MAX_EV);
    if (epoll_fd == -1)
        perror("Epoll fd creation failed");

    event.events = EPOLLIN;
    event.data.fd = socket;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket, &event) < 0)
    {
        close(epoll_fd);
        perror("epoll_ctl add failed");
    }

    while (true)
    {
        event_count = epoll_wait(epoll_fd, events, UDP_EPOLL_MAX_EV, 1000);
        if (event_count == 0)
            continue;
        for (i = 0; i < event_count; ++i)
            receive_udp_message(socket, config);
    }
}

/*
 *  Receive a message through the socket, directly sent to the epoll server so
 *  it can be parsed
 */
void receive_udp_message(int socket, server_config *config)
{
    char input[UDP_MTU + 1];
    struct sockaddr_in client;
    socklen_t c = sizeof(struct sockaddr_in);

    ssize_t size = recvfrom(socket, input, UDP_MTU, 0, &client, &c);
    if (size > 0)
    {
        // Parse request call here

        struct dns_message query = parse_dns_query(input);

        struct dns_message response = build_dns_response(config->tree, query);
        size_t sizeResp = 0;
        void *bits = encode_response(response, &sizeResp);
        // TODO for now send back request
        sendto(socket, bits, sizeResp, 0, &client, c);

        free(bits);
        free_dns_request(query);
        // sendto(socket, input, size, 0, &client, c);
    }
}
