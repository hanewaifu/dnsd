#define _GNU_SOURCE

#include "server/socket.h"

#include <err.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "socket.h"
/*
 *  Wrapper around fcntl to make a non-blocking socket
 */

void sock_set_non_blocking(int socket)
{
    int flags = fcntl(socket, F_GETFL);
    fcntl(socket, F_SETFL, flags | O_NONBLOCK);
}

/*
 *  Setting up a socket depending on the protocol used (TCP/UDP). It also
 *  checks syscall return values
 */

int get_addrinfo(server_config *config, T_PROTOCOL proto)
{
    struct addrinfo *res = NULL, *rp = NULL;

    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;

    int port = -1;
    if (proto == UDP)
    {
        hints.ai_socktype = SOCK_DGRAM;
        port = config->udp_port;
    }
    else if (proto == TCP)
    {
        hints.ai_socktype = SOCK_STREAM;
        port = config->tcp_port;
    }

    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    char snum[32];
    sprintf(snum, "%d", port);
    int s = getaddrinfo(config->ip, snum, &hints, &res);
    if (s != 0)
        err(1, "getaddrinfo error");

    int sockfd = -1;
    for (rp = res; rp != NULL; rp = rp->ai_next)
    {
        sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        int optval = 1;
        setsockopt(sockfd,
                   SOL_SOCKET,
                   SO_REUSEADDR | SO_REUSEPORT,
                   &optval,
                   sizeof(int));
        if (sockfd == -1)
            continue;
        if (bind(sockfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break;
        close(sockfd);
    }
    freeaddrinfo(res);
    if (rp == NULL)
        err(1, "no socket available");

    return sockfd;
}
