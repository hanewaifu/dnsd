#ifndef DNSD_REQUESTS_H
#define DNSD_REQUESTS_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/socket.h>
#include <tree/tree.h>
#include <unistd.h>

/**
** DNS Header
*/
struct dns_header
{
    uint16_t query_id;
    uint16_t flags;
    uint16_t questions_count;
    uint16_t answers_count;
    uint16_t author;
    uint16_t additional;
};

enum qtype
{
    A = 1,
    NS = 2,
    SOA = 6,
    AAAA = 28
};
/*
 * DNS question section
 */
struct dns_question
{
    char *qname;
    enum qtype qtype;
    uint16_t qclass;
};

/*struct resource_record
{
    char *name;
    uint16_t type;
    uint16_t class;
    uint32_t ttl;
    uint16_t rd_length;
    char* rdata;
};*/

enum Opcode
{
    QUERY = 0,
    IQUERY = 1,
    STATUS = 2
};

struct query_flags
{
    int qr;
    enum Opcode op_code;
    int is_authority;
    int is_truncated;
    int is_recursion_desired;
    int is_recursion_available;
    int is_auth_by_server;
    int response_code;
};

struct dns_message
{
    struct dns_header header;
    struct dns_question *questions;
    struct record *answer;
    struct record *authority;
    struct record *additional;
    struct query_flags flags;
};

struct dns_message parse_dns_query(char *query_str);

struct dns_question *parse_dns_questions(char **question_section,
                                         uint16_t question_count);
struct record *parse_records(char *query, char **cursor, uint16_t record_count);
void header_ntohs(struct dns_header **header);
void free_dns_request(struct dns_message query);

#endif /* end of include guard: DNSD_REQUESTS_H */
