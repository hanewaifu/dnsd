#ifndef REQUESTS_CRAFTER_H
#define REQUESTS_CRAFTER_H

#include "dnsd_requests.h"

char *craft_request(char *name, enum qtype type, size_t *buffer_len);

#endif /* end of include guard: REQUESTS_CRAFTER_H */
