#include "dnsd_requests.h"

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_set(uint16_t n, int k)
{
    return (n & (1 << k)) != 0;
}

struct query_flags parse_flags(uint16_t flags)
{
    struct query_flags res;
    res.qr = is_set(flags, 15);
    res.op_code = is_set(flags, 13);
    res.is_authority = is_set(flags, 10);
    res.is_truncated = is_set(flags, 9);
    res.is_recursion_desired = is_set(flags, 8);
    res.is_recursion_available = is_set(flags, 7);
    res.is_auth_by_server = is_set(flags, 5);
    return res;
}

struct dns_message parse_dns_query(char *query_str)
{
    struct dns_header *header = (struct dns_header *) query_str;
    char *cursor = query_str + sizeof(struct dns_header);

    header_ntohs(&header);

    struct query_flags flags = parse_flags(header->flags);

    struct dns_question *questions =
        parse_dns_questions(&cursor, header->questions_count);

    struct record *answers =
        parse_records(query_str, &cursor, header->answers_count);
    struct record *auth = parse_records(query_str, &cursor, header->author);
    struct record *additionals =
        parse_records(query_str, &cursor, header->additional);

    struct dns_message result;
    result.header = *header;
    result.questions = questions;
    result.flags = flags;
    result.answer = answers;
    result.authority = auth;
    result.additional = additionals;
    return result;
}

void header_ntohs(struct dns_header **header)
{
    (*header)->query_id = ntohs((*header)->query_id);
    (*header)->flags = ntohs((*header)->flags);
    (*header)->questions_count = ntohs((*header)->questions_count);
    (*header)->answers_count = ntohs((*header)->answers_count);
    (*header)->author = ntohs((*header)->author);
    (*header)->additional = ntohs((*header)->additional);
}

void put_dots(char *name)
{
    int len = strlen(name);
    for (int i = 0; i < len; i++)
        if ((name[i] < 97 || name[i] > 122) && (name[i] < 30 || name[i] > 39))
            name[i] = '.';
    for (int i = 0; i < len - 1; i++)
        name[i] = name[i + 1];
    name[len - 1] = '\0';
}

struct dns_question *parse_dns_questions(char **question_section,
                                         uint16_t question_count)
{
    struct dns_question *questions =
        malloc(sizeof(struct dns_question) * question_count + 4);
    for (uint16_t i = 0; i < question_count; i++)
    {
        struct dns_question q;
        int len = strlen((*question_section));
        char *qname = calloc(1, len + 1);
        for (int j = 0; j < len; j++)
            qname[j] = (*question_section)[j];
        q.qname = qname;
        // put_dots(q.qname);
        *question_section += len + 1;
        uint16_t *ptr = (uint16_t *) *question_section;
        q.qtype = ntohs(*ptr);
        ptr = (uint16_t *) (*question_section + sizeof(uint16_t));
        q.qclass = ntohs(*ptr);
        *question_section += 2 * sizeof(uint16_t);
        questions[i] = q;
    }
    return questions;
}

struct record *parse_records(char *query, char **cursor, uint16_t record_count)
{
    struct record *res = NULL;
    struct record *prev = NULL;
    struct record *rec = NULL;
    for (uint16_t i = 0; i < record_count; i++)
    {
        rec = calloc(1, sizeof(struct record));

        char *name_ptr;
        if (**cursor == -64)
        {
            char *offset = *cursor + 1;

            name_ptr = query + *offset;
        }
        else
            name_ptr = *cursor;

        int len = strlen(name_ptr);
        char *name = calloc(1, len + 1);

        for (int j = 0; j < len; j++)
            name[j] = name_ptr[j];
        rec->name = name;
        rec->name[len] = 0;
        // put_dots(rec->name);

        *cursor += (**cursor == -64 ? 2 : len + 1);

        uint16_t *ptr = (uint16_t *) *cursor;
        rec->type = ntohs(*ptr);
        *cursor += sizeof(uint16_t);

        ptr = (uint16_t *) *cursor;
        rec->class = ntohs(*ptr);
        *cursor += sizeof(uint16_t);

        uint32_t *ttl_ptr = (uint32_t *) *cursor;
        rec->TTL = ntohs(*ttl_ptr);
        *cursor += sizeof(uint32_t);

        ptr = (uint16_t *) *cursor;
        rec->len = ntohs(*ptr);
        *cursor += sizeof(uint16_t);

        // size_t i = 0;
        // for (; i < len; ++i)
        // {
        //    size_t t = rec->name[i];
        //    for (size_t j  = 0; j < t; ++j, ++i)
        //        rec->name[i] = rec->name[i + 1];
        //    rec->name[i] = '.';
        // }
        // rec->name[i - 1] = 0;

        char *data = calloc(1, rec->len + 1);
        for (int k = 0; k < rec->len; k++)
            data[k] = (*cursor)[k];
        rec->data = data;
        *cursor += rec->len;

        if (res == NULL)
        {
            res = rec;
            prev = rec;
        }
        else
        {
            prev->next = rec;
            prev = rec;
        }
    }
    return res;
}

void clear_record(struct record *record)
{
    if (record == NULL)
        return;

    clear_record(record->next);
    free(record->data);
    free(record->name);
    free(record);
}

void free_dns_request(struct dns_message query)
{
    for (int i = 0; i < query.header.questions_count; i++)
    {
        free(query.questions[i].qname);
    }
    free(query.questions);
    clear_record(query.answer);
    clear_record(query.authority);
    clear_record(query.additional);
}
