#include "requests_crafter.h"

#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>

struct dns_header_crafter
{
    uint16_t id;

    uint8_t rd : 1;
    uint8_t tc : 1;
    uint8_t aa : 1;
    uint8_t opcode : 4;
    uint8_t qr : 1;

    uint8_t rcode : 4;
    uint8_t cd : 1;
    uint8_t ad : 1;
    uint8_t z : 1;
    uint8_t ra : 1;

    uint16_t q_count;
    uint16_t ans_count;
    uint16_t auth_count;
    uint16_t add_count;
};

char *craft_request(char *name, enum qtype type, size_t *buffer_len)
{
    size_t name_len = strlen(name);
    *buffer_len = sizeof(struct dns_header_crafter) + name_len * sizeof(char)
                  + 2 * sizeof(uint16_t) + 4;

    char *buffer = calloc(*buffer_len, sizeof(char));

    struct dns_header_crafter *header = (struct dns_header_crafter *) buffer;

    header->id = rand() % 4096;
    header->qr = 0;
    header->opcode = 0;
    header->rd = 0;
    header->tc = 0;
    header->aa = 0;
    header->cd = 0;
    header->ad = 1;
    header->z = 0;
    header->ra = 0;
    header->q_count = htons(1);
    header->ans_count = 0;
    header->auth_count = 0;
    header->add_count = 0;

    char *qname = buffer + sizeof(struct dns_header_crafter);

    size_t word_len = 0;
    for (; word_len < name_len && name[word_len] != '.'; ++word_len)
        ;
    *qname++ = word_len;
    word_len = 0;
    for (size_t i = 0; i < name_len; ++i)
    {
        if (name[i] == '.')
        {
            for (; i + word_len + 1 < name_len && name[i + word_len + 1] != '.';
                 ++word_len)
                ;
            *qname++ = word_len;
            word_len = 0;
        }
        else
        {
            *qname++ = name[i];
        }
    }
    uint16_t *qtype = (uint16_t *) qname;
    *qtype = htons(type);
    uint16_t *qclass = qtype + sizeof(uint16_t);
    *qclass = 1;

    return buffer;
}
