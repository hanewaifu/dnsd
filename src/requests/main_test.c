#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dnsd_requests.h"

int main()
{
    char dns_query[] =
        /*"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x45\x00\x00\x3c\x51\xe3\x40\x00\x40\x11\xea\xcb\x7f\x00\x00\x01\x7f\x00\x00\x01\xec\xed\x00\x35\x00\x28\xfe\x3b"*/
        "\x24\x1a\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x03\x77\x77\x77\x06\x67\x6f\x6f\x67\x6c\x65\x03\x63\x6f\x6d\x00\x00\x01\x00\x01";
    // char dns_resp[] = "\x24\x1a\x81\x80\x00\x01
    // \x00\x03\x00\x00\x00\x00\x03\x77\x77\x77\x06\x67\x6f\x6f\x67\x6c\x65\x03\x63\x6f\x6d\x00\x00\x01\x00\x01\xc0\x0c\x00\x05\x00\x01\x00\x05\x28\x39\x00\x12\x03\x77\x77\x77\x01\x6c\x06\x67\x6f\x6f\x67\x6c\x65\x03\x63\x6f\x6d\x00\xc0\x2c\x00\x01\x00\x01\x00\x00\x00\xe3\x00\x04\x42\xf9\x59\x63\xc0\x2c\x00\x01\x00\x01\x00\x00\x00\xe3\x00\x04\x42\xf9\x59\x68";
    struct dns_message parsed = parse_dns_query(dns_query);
    if (parsed.header.query_id != 0x241a)
    {
        printf("Bad id\n");
        return EXIT_FAILURE;
    }

    if (parsed.flags.qr != 0)
    {
        printf("Bad flag qr\n");
        return EXIT_FAILURE;
    }

    if (parsed.header.questions_count != 1)
    {
        printf("Bad question count\n");
        return EXIT_FAILURE;
    }

    if (parsed.header.answers_count != 0)
    {
        printf("Bad answer count\n");
        return EXIT_FAILURE;
    }

    if (strcmp(parsed.questions[0].qname, "wwwgooglecom") == 0)
    {
        printf("Bad question name\n");
        return EXIT_FAILURE;
    }

    if (parsed.questions[0].qtype != A)
    {
        printf("Bad question type: %d\n", parsed.questions[0].qtype);
        return EXIT_FAILURE;
    }

    if (parsed.questions[0].qclass != 1)
    {
        printf("Bad question class\n");
        return EXIT_FAILURE;
    }
    free_dns_request(parsed);

    return EXIT_SUCCESS;
}
