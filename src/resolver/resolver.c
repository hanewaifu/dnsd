#include "resolver.h"

#include <arpa/inet.h>
#include <err.h>
#include <netdb.h>
#include <netinet/in.h>
#include <requests/requests_crafter.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

struct dns_message dns_query(char *name, enum qtype type, char *server_ip)
{
    size_t buffer_len;
    char *buffer1 = craft_request(name, type, &buffer_len);

    int sck = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(53);
    serv_addr.sin_addr.s_addr = inet_addr(server_ip);

    connect(sck, (const struct sockaddr *) &serv_addr, sizeof(serv_addr));

    uint16_t buff_len = htons(buffer_len);
    write(sck, &buff_len, sizeof(uint16_t));
    write(sck, buffer1, buffer_len);

    read(sck, &buff_len, 2);
    buff_len = ntohs(buff_len);
    char *buffer2 = malloc(sizeof(char) * buff_len);
    read(sck, buffer2, buff_len);

    close(sck);
    free(buffer1);

    struct dns_message dns_res = parse_dns_query(buffer2);
    free(buffer2);
    return dns_res;
}

struct resource_record *
simple_resolver(char *name, enum qtype type, struct tree *tree)
{
    struct record *record;
    int record_size = get_record(tree, name, type, &record);

    if (record_size)
    { // found
    }
    else
    {
        record_clear(record);
    }
    return NULL;
}

void resolve(struct dns_message query, struct tree *tree)
{
    tree = tree;

    // TODO: response struct

    for (size_t i = 0; i < query.header.questions_count; ++i)
    {
    }
}
