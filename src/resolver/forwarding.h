#ifndef FORWARDING_H
#define FORWARDING_H

#include <requests/dnsd_requests.h>
#include <tree/tree.h>

char *forward_request(struct dns_message query, char *server_ip);

#endif /* !FORWARDING_H */
