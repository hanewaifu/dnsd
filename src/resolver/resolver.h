#ifndef RESOLVER_H
#define RESOLVER_H

#include <requests/dnsd_requests.h>
#include <tree/tree.h>

struct dns_message dns_query(char *name, enum qtype type, char *server_ip);

void resolve(struct dns_message query, struct tree *tree);

#endif /* !RESOLVER_H */
