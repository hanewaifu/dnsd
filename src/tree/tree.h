#ifndef TREE_H
#define TREE_H

#include <stddef.h>
#include <stdint.h>

struct record
{
    struct record *next;
    uint16_t type;
    uint16_t class;
    uint32_t TTL;
    void *data;
    uint16_t len;
    char *name;
};

struct tree_item
{
    struct tree_item *next;
    char *domain;
    int nb_records;
    struct record *first_record;
    struct tree_item *first_item;
};

struct tree
{
    struct tree_item *root;
};

struct tree *tree_init(void);
struct record *record_init(uint16_t type,
                           uint16_t class,
                           uint32_t TTL,
                           void *data,
                           char *name,
                           size_t data_len);
void tree_clear(struct tree *tree);
void tree_free(struct tree *tree);
int get_records(struct tree *tree, char *name, struct record **records);
int get_record(struct tree *tree, char *name, int type, struct record **result);
int name_split(char *name, char ***words);
void record_clear(struct record *record);
int add_record(struct tree *tree, char *name, struct record *record);
int add_record_root(struct tree *tree, struct record *record);

#endif // TREE_H
