#include "tree.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 *  Init tree with the root
 */

struct tree *tree_init(void)
{
    struct tree *new = malloc(sizeof(struct tree));

    struct tree_item *root = calloc(1, sizeof(struct tree_item));
    root->domain = ".";
    new->root = root;

    return new;
}

/*
 *  Constructor for a record
 */

struct record *record_init(uint16_t type,
                           uint16_t class,
                           uint32_t TTL,
                           void *data,
                           char *name,
                           size_t data_len)
{
    struct record *new = calloc(1, sizeof(struct record));
    new->type = type;
    new->TTL = TTL;
    new->class = class;
    new->name = malloc(strlen(name) * sizeof(char) + 1);
    strcpy(new->name, name);

    new->len = data_len;
    new->data = malloc(data_len + 1);
    uint8_t *bits1 = new->data;
    uint8_t *bits2 = data;
    for (size_t i = 0; i < data_len; ++i)
    {
        bits1[i] = bits2[i];
    }
    return new;
}

void record_clear(struct record *record)
{
    if (!record)
        return;

    record_clear(record->next);
    free(record->data);
    free(record->name);
    free(record);
}

/*
 *  Recursively free all item at same level in the tree
 */

static void tree_item_clear(struct tree_item *item)
{
    if (!item)
        return;

    tree_item_clear(item->next);
    tree_item_clear(item->first_item);
    record_clear(item->first_record);
    free(item->domain);
    free(item);
}

/*
 *  Empty the tree
 */

void tree_clear(struct tree *tree)
{
    if (!tree || !tree->root)
        return;

    tree_item_clear(tree->root->first_item);
}

/*
 *  Empty the tree and free it
 */

void tree_free(struct tree *tree)
{
    if (!tree)
        return;

    tree_clear(tree);

    if (!tree->root)
        return;

    free(tree->root);
    free(tree);
}

/*
 *  Slit the string into labels to be used as a path in the tree
 */

int name_split(char *name, char ***words)
{
    int nb_points = 0;

    for (size_t i = 0; i < strlen(name); ++i)
        if (name[i] == '.')
            ++nb_points;

    int size = nb_points + 1;
    *words = malloc((size) * sizeof(char *));

    char *word = strtok(name, ".");

    while (word)
    {
        (*words)[nb_points] = malloc(strlen(word) * sizeof(char *));
        strcpy((*words)[nb_points], word);
        nb_points--;
        word = strtok(NULL, ".");
    }

    return size;
}

/*
 *  Search a label in a level of the tree
 */

struct tree_item *search_item(struct tree_item *items, char *domain)
{
    if (!items)
        return NULL;

    struct tree_item *previous = items;

    while (items)
    {
        previous = items;

        if (!strcmp(items->domain, domain))
            return items;

        items = items->next;
    }

    return previous;
}

/*
 *  Get all records for an address, no matter the type
 */

int get_records(struct tree *tree, char *name, struct record **records)
{
    if (!name)
    {
        fprintf(stderr, "Error: No name specified\n");
        return -1;
    }

    if (!tree)
    {
        fprintf(stderr, "Error: No tree given\n");
        return -1;
    }

    char **words;
    int size = name_split(name, &words);

    struct tree_item *item = tree->root;

    for (int i = 0; i < size; ++i)
    {
        item = search_item(item->first_item, words[i]);

        if (!item || strcmp(item->domain, words[i]))
        {
            for (int j = i; j < size; ++j)
                free(words[j]);
            free(words);
            return -1;
        }

        free(words[i]);
    }

    free(words);

    int nb_records = item->nb_records;
    *records = calloc(nb_records, sizeof(struct record));
    struct record *record = item->first_record;
    int i = 0;

    while (record)
    {
        (*records)[i] = *record;
        record = record->next;
        ++i;
    }

    return nb_records;
}

/*
 *  Get all records for an address, corresponding to the specified type
 */

int get_record(struct tree *tree, char *name, int type, struct record **result)
{
    struct record *records;
    int size = get_records(tree, name, &records);
    int nb_records = 0;

    for (int i = 0; i < size; i++)
    {
        if (records->type == type)
        {
            struct record *curr = calloc(1, sizeof(struct record));
            memcpy(curr, records, sizeof(struct record));
            curr->next = *result;
            *result = curr;
            ++nb_records;
        }

        records = records->next;
    }

    return nb_records;
}

/*
 *  Populate the tree with a new record
 */

int add_record(struct tree *tree, char *name, struct record *record)
{
    if (!name)
    {
        fprintf(stderr, "Error: No name specified\n");
        return 1;
    }

    if (!tree)
    {
        fprintf(stderr, "Error: No tree given\n");
        return 1;
    }

    if (!record)
    {
        fprintf(stderr, "Error: No record given\n");
        return 1;
    }

    char **words;
    int size = name_split(name, &words);

    struct tree_item *curr = tree->root;
    struct tree_item *item = NULL;

    for (int i = 0; i < size; ++i)
    {
        item = search_item(curr->first_item, words[i]);

        if (item && !(strcmp(item->domain, words[i])))
        {
            curr = item;
            free(words[i]);
            continue;
        }

        struct tree_item *new = calloc(1, sizeof(struct tree_item));
        new->domain = words[i];

        if (!item)
            curr->first_item = new;

        else
            item->next = new;

        curr = new;
    }

    free(words);

    ++curr->nb_records;
    record->next = curr->first_record;
    curr->first_record = record;

    return 0;
}

/*
 *  Add a new record to the root
 */

int add_record_root(struct tree *tree, struct record *record)
{
    record->next = tree->root->first_record;
    tree->root->first_record = record;
    return 0;
}
